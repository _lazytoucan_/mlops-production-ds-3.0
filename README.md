# MLOps и production в DS исследованиях 3.0

## Описание
Данный проект создан для изучения современных подходов и инструментов для исследований на данных, разработки и внедрения ML-решений в production по открытому курсу [MLOps и production в DS исследованиях 3.0](https://ods.ai/tracks/mlops3-course-spring-2024)

## Методология
Методология проекта - [GitHub flow](https://docs.github.com/en/get-started/using-github/github-flow)

## Инструкция
Для запуска необходимо 
* Скачать, установить и запустить [Docker](https://www.docker.com/) 
* Cкачать репозиторий 
* С помощью команды `docker build -t my_container` запустить создание образа на основе Dockerfile
* После скачивания и установки выполнить команду `docker run -p 8888:8888 my_container`. 

По итогу запустится контейнер с Jupyter notebook, доступ к которому можно получить из браузера по [localhost](http://127.0.0.1:8888/tree)

Если необходимо установить зависимости для разработки, необходимо в файле Dockerfile убрать `--without dev`


