FROM jupyter/base-notebook:x86_64-ubuntu-22.04

WORKDIR /app

RUN pip install poetry==1.8.2
COPY  pyproject.toml poetry.lock ./

RUN poetry install --without dev

CMD [ "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root" ]
