## Линтеры

В проекте используются следующие линтеры:
 - [ ] [mypy](https://mypy.readthedocs.io/en/stable/)
 - [ ] [Ruff](https://docs.astral.sh/ruff/)
 - [ ] [pre-commit-hooks](https://github.com/pre-commit/pre-commit-hooks)

Все они описаны в файле .pre-commit-config.yaml 

Для их использования необходимо установить библиотеку pre-commit:
```
pip install pre-commit
```

Далее, для загрузки и инициализации линтеров необходимо выполнить команду:
```
pre-commit
```
После успешного выполнения при коммите и пуше будут автоматически запускаться линтеры, описанные выше